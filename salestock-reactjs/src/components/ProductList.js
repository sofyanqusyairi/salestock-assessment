import React, { Component } from 'react'
import ProductItem from './ProductItem'

export default class ProductList extends Component {
  constructor(props) {
    super(props)
    this.data = []
  }

  shouldComponentUpdate(nextProps, nextState){
    return (JSON.stringify(nextProps.data) !== JSON.stringify(this.props.data))
  }

  _init() {
    if (this.props.data !== undefined) {
      this.data = this._createGroupedArray(this.props.data, 3)
    }
  }

  _createGroupedArray(arr, chunkSize) {
    var groups = [], i
    for (i = 0; i < arr.length; i += chunkSize) {
      groups.push(arr.slice(i, i + chunkSize))
    }
    return groups;
  }

  render() {
    this._init()

    var _renderProductItem = () => {
      return (
        <div>
          {
            this.data.map((items, i) => {
              return (
                <div key={i} className={'inline'}>
                  {
                    items.map((item, index) => {
                      return (
                        <div key={index}>
                          <ProductItem item={item} />
                        </div>
                      )
                    })
                  }
                </div>
              )
            })
          }
        </div>
      )
    }
    return (
      <div className={'productListContainer'}>
        {_renderProductItem()}
      </div>
    )
  }
}