import React, { Component } from 'react'
import ProductItemDetail from './ProductItemDetail'

export default class ProductItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isShowDetail: false
    }
    this.item = {
      title: '',
      price: '',
      availableColor: '',
      availableSize: '',
      description: '',
      publishedDate: '',
      image: '',
    }

    this._handleShowDetail = this._handleShowDetail.bind(this)
    this._handleCloseDetail = this._handleCloseDetail.bind(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (JSON.stringify(nextState) !== JSON.stringify(this.state)) return true
    return (JSON.stringify(nextProps.item) !== JSON.stringify(this.props.item))
  }

  _init() {
    if (JSON.stringify(this.props.item) !== JSON.stringify(this.item)) {
      this.item = this.props.item
    }
  }

  _handleShowDetail() {
    this.setState({
      isShowDetail: true
    })
  }

  _handleCloseDetail() {
    this.setState({
      isShowDetail: false
    })
  }

  render() {
    this._init()

    var _renderAvailableColor = () => {
      let availableColor = this.item.availableColor.split(',')
      return (
        <div className={'inline'}>
          {
            availableColor.map((item, i) => {
              return (
                <div
                  key={i}
                  className={'availableColor'}
                  style={{
                    backgroundColor: item,
                    borderColor: '#F7F5FA',
                    borderWidth: 1
                  }} />
              )
            })
          }
        </div>
      )

    }

    var _renderAvailableSize = () => {
      return (
        <div>
          {this.item.availableSize}
        </div>
      )
    }

    return (
      <div>
        <div className={'productContainer'} onClick={this._handleShowDetail}>
          <div className={'thumbnailContainer'}>
            <img className={'productThumbnail'} src={this.item.image} alt={this.item.title} />
          </div>
          <div className={'productInfo'}>
            <div style={{ fontSize: 13, fontWeight: 'bold', }}>
              {this.item.title}
            </div>
            <div>
              {'Rp ' + this.item.price}
            </div>
            {_renderAvailableSize()}
            {_renderAvailableColor()}
          </div>
        </div>
        <div className={'modal-container'}>
          <ProductItemDetail
            isShow={this.state.isShowDetail}
            item={this.item}
            handleClose={this._handleCloseDetail} />
        </div>
      </div >
    )
  }
}