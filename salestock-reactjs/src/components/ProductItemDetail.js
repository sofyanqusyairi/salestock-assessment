import React, { Component } from 'react'
import { Modal } from 'react-bootstrap'

export default class ProductItemDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isImgFull: false
    }
    this.item = {
      title: '',
      price: '',
      availableColor: '',
      availableSize: '',
      description: '',
      publishedDate: '',
      image: '',
    }

    this._onShowImgFull = this._onShowImgFull.bind(this)
    this._onCloseImgFull = this._onCloseImgFull.bind(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (JSON.stringify(nextState) !== JSON.stringify(this.state)) return true
    return ((JSON.stringify(nextProps.item) !== JSON.stringify(this.props.item) ||
      (JSON.stringify(nextProps.isShow) !== JSON.stringify(this.props.isShow))))
  }

  _init() {
    if (JSON.stringify(this.props.item) !== JSON.stringify(this.item)) {
      this.item = this.props.item
    }
  }

  _onShowImgFull() {
    this.setState({ isImgFull: true })
  }

  _onCloseImgFull() {
    this.setState({ isImgFull: false })
  }

  render() {
    this._init()

    var _renderAvailableColor = () => {
      let availableColor = this.item.availableColor.split(',')
      return (
        <div className={'inline'}>
          {
            availableColor.map((item, i) => {
              return (
                <div
                  key={i}
                  className={'availableColor'}
                  style={{
                    backgroundColor: item,
                    borderColor: '#F7F5FA',
                    borderWidth: 1
                  }} />
              )
            })
          }
        </div>
      )

    }

    var _renderAvailableSize = () => {
      return (
        <div>
          {this.item.availableSize}
        </div>
      )
    }

    return (
      <div>
        <Modal show={this.props.isShow} onHide={this.props.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>{this.item.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className={'inline'}>
              <img
                className={'productThumbnail'}
                src={this.item.image}
                onClick={this._onShowImgFull}
                alt={this.item.title} />
              <div className={'productDetail'}>
                <div style={{ fontSize: 20, fontWeight: 'bold', }}>
                  {'Rp ' + this.item.price}
                </div>
                <div style={{ fontSize: 13 }}>
                  {_renderAvailableSize()}
                </div>
                <div>
                  {_renderAvailableColor()}
                </div>
                <div style={{ fontSize: 10 }}>
                  {this.item.publishedDate}
                </div>
              </div>
            </div>
            <div>
              <div style={{ fontSize: 15, fontWeight: 'bold', marginTop: 10 }}>
                DESKRIPSI:
              </div>
              <div dangerouslySetInnerHTML={{ __html: this.item.description }} />
            </div>
          </Modal.Body>
        </Modal>
        <Modal
          show={this.state.isImgFull} onHide={this._onCloseImgFull}>
          <Modal.Header closeButton />
          <Modal.Body>
            <div className={'productThumbnailFullContainer'}>
              <img className={'productThumbnailFull'} src={this.item.image} alt={this.item.title} />
            </div>
          </Modal.Body>
        </Modal>
      </div>
    )
  }
}