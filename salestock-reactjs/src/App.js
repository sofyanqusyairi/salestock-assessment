import React, { Component } from 'react';
import logo from './salestocklogo.png';
import './App.css';
import Product from './app/Product'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img className={'logo'} src={logo} alt={'logo'} />
          <h1 className="App-title">Selamat Datang di Sale Stock</h1>
        </header>
        <Product />
      </div>
    );
  }
}

export default App;
