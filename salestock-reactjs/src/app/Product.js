import React, { Component } from 'react'
import ProductList from '../components/ProductList'
import './Product.css'
import './bootstrap-3.3.7-dist/css/bootstrap.css'
const PRODUCT_URL = 'http://localhost:3001/api/product/list?'
const TIMEOUT = 1000
const PAGE = 1
const PAGESIZE = 18

export default class Product extends Component {
  constructor(props) {
    super(props)
    this.state = {
      totalProduct: 0,
      products: []
    }
    this.page = PAGE
    this.url = PRODUCT_URL

    this._trackScrolling = this._trackScrolling.bind(this)
    this._callAPI = this._callAPI.bind(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (JSON.stringify(nextState) !== JSON.stringify(this.state))
  }

  _changeURL() {
    this.url = PRODUCT_URL + `&page=${this.page}&pagesize=${PAGESIZE}`
  }

  _isBottom(el) {
    return el.getBoundingClientRect().bottom <= window.innerHeight;
  }

  async _trackScrolling() {
    const wrappedElement = document.getElementById('root')
    if (this._isBottom(wrappedElement)) {
      this.page++
      this._changeURL()
      // re-request API
      let resp = await this._callAPI(this.url)
      this.setState({
        products: [...this.state.products, ...resp.result]
      })
      // document.removeEventListener('scroll', this._trackScrolling)
    }
  };

  async componentDidMount() {
    document.addEventListener('scroll', this._trackScrolling)
    this._changeURL()
    let resp = await this._callAPI(this.url)
    this.setState({
      products: resp.result,
      totalProduct: resp.total
    })
  }

  async _callAPI(url) {
    let resp = await fetch(url, {
      method: 'GET',
      timeout: TIMEOUT
    })
    let respJson = await resp.json();
    return respJson
  }

  render() {
    return (
      <div className={'container'}>
        <div
          style={{
            fontSize: 15,
            fontWeight: 'bold',
            color: 'blue'
          }}>
          Total Product: {this.state.totalProduct}
        </div>
        <ProductList data={this.state.products} />
        <div
          className={'loader'}
          style={{
            visibility: this.state.products.length == this.state.totalProduct ? 'hidden' : 'visible'
          }}
        />
      </div>
    )
  }
}