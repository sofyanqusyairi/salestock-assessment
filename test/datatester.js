var mongoose = require('mongoose')

// connect to database
mongoose.connect('mongodb://localhost/salestock-cms')
data = []

total = 100

var ProductSchema = mongoose.Schema({
  title: String,
  price: String,
  availableColor: String,
  availableSize: String,
  description: String,
  publishedDate: {type: Date, default: (new Date).toString()},
  image: String
})

var Product = mongoose.model('Product', ProductSchema)

for (i=1;i<=total;i++){
  data.push({
    title: 'Gherisa Lace V-Neck Blouse'+i,
    price: (96000+i).toString(),
    availableColor: 'blue,red,green,yellow,orange,purple,black,white,pink,brown,gray,silver,gold,cyan',
    availableSize: 'S,M,L,XL,XXL',
    description: '<div class=\"markdown\" style=\"box-sizing: border-box; color: #4a4a4a; font-family: system, -apple-system, system-ui, \'Helvetica Neue\', Arial, sans-serif; flex: 1 1 0%;\">\r\n<p style=\"box-sizing: border-box; margin: 1em auto;\"><span style=\"box-sizing: border-box;\">Warna Maroon</span><br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box;\">Size S :</span><br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box;\">Lingkar dada 88 cm, Lebar bahu 35 cm, Panjang lengan 25 cm, Lingkar lengan 45 cm, Panjang 60 cm, Lingkar pinggang 90 cm</span><br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box;\">Size M :</span><br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box;\">Lingkar dada 92 cm, Lebar bahu 36 cm, Panjang lengan 25 cm, Lingkar lengan 46 cm, Panjang 60 cm, Lingkar pinggang 94 cm</span><br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box;\">Size L :</span><br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box;\">Lingkar dada 96 cm, Lebar bahu 37 cm, Panjang lengan 26 cm, Lingkar lengan 48 cm, Panjang 62 cm, Lingkar pinggang 98 cm</span><br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box;\">Size XL :</span><br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box;\">Lingkar dada 100 cm, Lebar bahu 38 cm, Panjang lengan 27 cm, Lingkar lengan 50 cm, Panjang 64 cm, Lingkar pinggang 102 cm</span><br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box;\">Size XXL :&nbsp;</span><br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box;\">Lingkar dada 116 cm, Lebar bahu 41 cm, Panjang lengan 28 cm, Lingkar lengan 56 cm, Panjang 68 cm, Lingkar pinggang 120 cm&nbsp;</span><br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box;\">Bahan Baby Peach kombinasi brukat</span><br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box;\">2 Kancing lengan variasiModel menggunakan size M</span></p>\r\n</div>\r\n<p><span style=\"box-sizing: border-box; color: #4a4a4a; font-family: system, -apple-system, system-ui, \'Helvetica Neue\, Arial, sans-serif;\">Produk bisa dicoba dan dikembalikan:&nbsp;Ya</span></p>',
    publishedDate: (new Date).toString(),
    image : 'https://salestock-public-prod.global.ssl.fastly.net/product_images/32f42b9a9bb15e1ad15891fa936184fd.jpg'

  },)
}

Product.insertMany(data, function(err, result){
  if (err) console.log('error:', err)
  else console.log('inserted')
})