var keystone = require('keystone');
var Types = keystone.Field.Types;

var Product = new keystone.List('Product');

Product.add({
  title: { type: String },
  price: { type: String },
  availableColor: { type: String, default: 'blue,red,green,yellow,orange,purple,black,white,pink,brown,gray,silver,gold,cyan' },
  availableSize: { type: String, default: 'S,M,L,XL,XXL' },
  description: { type: Types.Html, wysiwyg: true, height: 300 },
  publishedDate: { type: Types.Datetime, default: Date.now, options: { parseFormat: 'YYYY-MM-DD h:m:s a' } },
  image: { type: String, label: 'Image URL' }
});

Product.track = true;
Product.defaultSort = 'publishedDate';
Product.defaultColumns = 'title, publishedDate';
Product.register();
