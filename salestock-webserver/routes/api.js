var express = require('express')
var router = express.Router()
var mongoose = require('mongoose')

var ProductSchema = mongoose.Schema({
  title: String,
  price: String,
  availableColor: String,
  availableSize: String,
  description: String,
  publishedDate: String,
  image: String
})
var Product = mongoose.model('Product', ProductSchema)

router.get('/product/list', function (req, res) {
  /**
   * this path will handle query page & pagesize.
   * page: current page
   * pagesize: total data per-page
   */
  let page = 1
  let pageSize = 20
  let query = req.query
  try {
    page = parseInt(query.page)
    pageSize = parseInt(query.pagesize)
  }
  catch (e) {
    console.log(e)
  }
  let startIndex = (page - 1) * pageSize

  // response with result
  Product.find({})
    .sort([['publishedDate', 'descending']])
    .skip(startIndex)
    .limit(pageSize)
    .exec(function (err, products) {
      let result = []
      if (err || !products) return res.status(200).send({ status: 'notok', error: err })
      if (products) {
        result = products
      }
      Product.count({}, function (err, c) {
        if (err) return res.status(200).send({ status: 'notok', error: err })
        return res.status(200).send({
          status: 'ok',
          total: c,
          result: result,
        })
      })
    })
})

module.exports = router