# Minimum Spesification
Make sure you have Node.js 0.10+ and MongoDB v2.4+ installed.

# Installation
## simple install & run
1. do this -> sh deploy.sh

## After install & run (Important!)
1. CMS will be in port 3000. access page(:3000/)
2. Web Server will be in port 3001, access page(:3001/home/)
3. Admin user for you with the email = admin@test.com and the password = admin.

## manual install
*** No need this part (Only If you success do simple install & run).

You should do "npm install" inside salestock-cms, salestock-reactjs, & salestock-webserver folder.

### CMS
Content Management System (CMS) to manage data of product that will be show on UI, installation step : 
1. cd salestock-cms
2. npm install
3. node keystone.js

### REACTJS (CLIENT)
Using ReactJS as UI (frontend) for client side
1. npm install
2. run dev: npm start / run production: npm run build

### WEBSERVER
This part is webserver to serve API that will be use on client side.
Webserver will get data that created by CMS, then showing them.
installation step:
1. cd salestock-webserver
2. npm install
3. npm start

# TESTER (quite important)
I make data tester, to insert many data to database. So, we can test the functionality.
installation step:
1. cd test
2. npm install
3. node datatester.js
After that, you can show the web page or CMS

# Assumption
1. This page only use to see product list, and see the detail
2. I only use these information to show :
      title,
      price,
      availableColor,
      availableSize,
      description,
      publishedDate,
      image(URL)
3. User will interest about image, price, size, & color. so make these information to show inside Product List Item

# Limitation
1. On CMS, you will see some default value. But, we have limitation here, You should manually type the value of "availableSize" and "availableColor"

## example: 
1. availableColor: 'blue,red,green,yellow,orange,purple,black,white,pink,brown,gray,silver,gold,cyan'
2. availableSize: 'S,M,L,XL,XXL'

# NOTED
if you got this "Please check you are not already running a server on the specified port.",
 then please kill process on that port first. or you can just do 'killall node'